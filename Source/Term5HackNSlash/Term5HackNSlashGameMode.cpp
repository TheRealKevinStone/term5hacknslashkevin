// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "Term5HackNSlashGameMode.h"


ATerm5HackNSlashGameMode::ATerm5HackNSlashGameMode(const FObjectInitializer& ObjectInitializer)
:Super(ObjectInitializer)
{
    // Blueprint'/Game/PlayerMaw.PlayerMaw'
    // Structure to hold one-time Initialization
    struct FConstructorStatics
    {
        ConstructorHelpers::FObjectFinder<UBlueprint> DefaultCharacterAsset;
        FConstructorStatics()
            : DefaultCharacterAsset(TEXT("Blueprint'/Game/PlayerMaw.PlayerMaw'"))
        {
        }
    };
    
    static FConstructorStatics ConstructorStatic;
    // Property initialization
    if (ConstructorStatic.DefaultCharacterAsset.Object != NULL)
    {
        DefaultPawnClass = (UClass*)ConstructorStatic.DefaultCharacterAsset.Object->GeneratedClass;
    }
}

