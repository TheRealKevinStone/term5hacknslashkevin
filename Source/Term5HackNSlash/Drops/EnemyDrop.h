// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Player/PlayerCharacter.h"
#include "EnemyDrop.generated.h"

UCLASS()
class TERM5HACKNSLASH_API AEnemyDrop : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyDrop();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent* Collider;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;

	float EmissiveIntensity;
	float EmissiveIntensityFactor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UMaterial* DropMaterial;

	UMaterialInstanceDynamic* DynamicMaterial;

protected:
	UFUNCTION()
	virtual void OnOverlap(class AActor* OtherActor);

	UFUNCTION(BlueprintImplementableEvent, Category = Drop, meta=(FriendlyName="Apply to Player"))
	void Event_ApplyToPlayer(APlayerCharacter* Player);

	UFUNCTION()
	void AddImpulseOnSpawn();

};
