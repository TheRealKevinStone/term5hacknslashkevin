// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "EnemyDrop.h"


// Sets default values
AEnemyDrop::AEnemyDrop()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
	RootComponent = Collider;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->AttachTo(RootComponent);

	OnActorBeginOverlap.AddDynamic(this, &AEnemyDrop::OnOverlap);

	EmissiveIntensity = 0.f;
	EmissiveIntensityFactor = 5.f;

	AddImpulseOnSpawn();

	//UBoxComponent* col = Cast<UBoxComponent>();

}

// Called when the game starts or when spawned
void AEnemyDrop::BeginPlay()
{
	Super::BeginPlay();
	
	DynamicMaterial = UMaterialInstanceDynamic::Create(DropMaterial, this);
	Mesh->SetMaterial(0, DynamicMaterial);

	FVector Impulse = FVector(0.f, 0.f, 40000.f);
	Collider->AddImpulse(Impulse);
}

// Called every frame
void AEnemyDrop::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (EmissiveIntensity >= 10)
	{
		EmissiveIntensityFactor = -5.f;
	}
	else if (EmissiveIntensity <= 0.f)
	{
		EmissiveIntensityFactor = 5.f;
	}

	EmissiveIntensity += EmissiveIntensityFactor * DeltaTime;
	GEngine->AddOnScreenDebugMessage(1, 1.5f, FColor::Red, FString::Printf(TEXT("EmissiveIntensity: %f"), EmissiveIntensity));
	//UMaterialInstanceDynamic* Material = Cast<UMaterialInstanceDynamic>(Mesh->GetMaterial(0));
	DynamicMaterial->SetVectorParameterValue(TEXT("EmissiveColor"), FLinearColor(EmissiveIntensity, EmissiveIntensity, 0.f, 1.f));
}

void AEnemyDrop::OnOverlap(class AActor* OtherActor)
{
	APlayerCharacter* Player = Cast<APlayerCharacter>(OtherActor);
	if (Player)
	{
		//GEngine->AddOnScreenDebugMessage(2, 1.5f, FLinearColor::Red, TEXT("XP Gained!"));
		Event_ApplyToPlayer(Player);
		Destroy();
	}
}

void AEnemyDrop::AddImpulseOnSpawn()
{
	/*FVector Impulse = FVector(0.f, 0.f, 100000.f);
	Collider->AddImpulse(Impulse, FName(), true);*/
}
