// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "Enemy.h"
#include "EnemyAnimInstance.h"

void UEnemyAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
	//Super::NativeUpdateAnimation(<#DeltaTime#>);

	AEnemy* Owner = Cast<AEnemy>(TryGetPawnOwner());

	if (Owner)
	{
		if (Owner->TakingDamage())
		{
			bTakingDamage = true;
		}
		if (Owner->IsDead())
		{
			bIsDead = true;
		}
		if (bDestroyNow)
		{
			Owner->bDestroyNow = true;
		}
	}
}


