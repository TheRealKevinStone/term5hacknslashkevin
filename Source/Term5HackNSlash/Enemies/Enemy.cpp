// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "Enemy.h"
#include "UnrealNetwork.h"
#include "EnemyAnimInstance.h"


// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Health = 50.f;

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AEnemy::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

	if (bDestroyNow)
	{
		Destroy();
	}
}

float AEnemy::TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);

	/*UEnemyAnimInstance* AnimInstance = Cast<UEnemyAnimInstance>(GetMesh()->GetAnimInstance());
	AnimInstance->bTakenDamage = true;*/

	Health -= DamageAmount;
	RecieveDamage();

	if (Health <= 0.f)
	{
		// Play Death Animation
		// For now just destory
		Die();
	}
	else
	{
		// Do Damage Animation
		// GEngine->AddOnScreenDebugMessage(1, 5.f, FLinearColor::Yellow, TEXT("OWW!"));
	}

	return ActualDamage;
}

void AEnemy::RecieveDamage()
{
	if (bIsDamaged == true)
	{
		bIsDamaged = false;
	}
	else
	{
		bIsDamaged = true;
	}
	Server_RecieveDamage(bIsDamaged);
}

bool AEnemy::Server_RecieveDamage_Validate(bool bClientIsDamaged)
{
	return true;
}

void AEnemy::Server_RecieveDamage_Implementation(bool bClientIsDamaged)
{
	bIsDamaged = bClientIsDamaged;
}

void AEnemy::Die()
{
	bIsDead = true;
	Server_Die(bIsDead);

	if (DropClass != NULL)
	{
		GetWorld()->SpawnActor<AEnemyDrop>(DropClass, GetActorLocation(), FRotator::ZeroRotator);
	}

	Destroy();
}

bool AEnemy::Server_Die_Validate(bool bClientIsDead)
{
	return true;
}

void AEnemy::Server_Die_Implementation(bool bClientIsDead)
{
	bIsDead = bClientIsDead;
}

void AEnemy::GetLifetimeReplicatedProps(TArray <FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(AEnemy, bIsDead);
	DOREPLIFETIME(AEnemy, bIsDamaged);

}