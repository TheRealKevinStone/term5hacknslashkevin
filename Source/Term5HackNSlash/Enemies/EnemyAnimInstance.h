// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "EnemyAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class TERM5HACKNSLASH_API UEnemyAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bTakingDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bIsDead;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool bDestroyNow;
public:
	virtual void NativeUpdateAnimation(float DeltaTime) override;
	
};
