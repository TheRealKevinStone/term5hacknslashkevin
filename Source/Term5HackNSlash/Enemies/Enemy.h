// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "Drops/EnemyDrop.h"
#include "Enemy.generated.h"

UCLASS()
class TERM5HACKNSLASH_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const &DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;
	
	bool TakingDamage(){ return bIsDamaged; }
	bool IsDead(){ return bIsDead; }
	bool bDestroyNow;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<AEnemyDrop> DropClass;

	float Health;

	UPROPERTY(Replicated)
	bool bIsDamaged;

	UPROPERTY(Replicated)
	bool bIsDead;

protected:
	void RecieveDamage();
	UFUNCTION(Server, WithValidation, Reliable)
		virtual void Server_RecieveDamage(bool bClientIsDamaged);
	virtual bool Server_RecieveDamage_Validate(bool bClientIsDamaged);
	virtual void Server_RecieveDamage_Implementation(bool bClientIsDamaged);

	virtual void Die();
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void Server_Die(bool bClientIsDead);
	virtual bool Server_Die_Validate(bool bClientIsDead);
	virtual void Server_Die_Implementation(bool bClientIsDead);


};
