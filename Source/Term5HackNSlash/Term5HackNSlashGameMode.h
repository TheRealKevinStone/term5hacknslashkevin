// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "Term5HackNSlashGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TERM5HACKNSLASH_API ATerm5HackNSlashGameMode : public AGameMode
{
	GENERATED_BODY()
	
public:
    ATerm5HackNSlashGameMode(const FObjectInitializer& ObjectInitializer);
	
	
};
