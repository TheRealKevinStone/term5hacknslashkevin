// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Character.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Weapons/Weapon.h"
#include "PlayerCharacter.generated.h"

UCLASS()
class TERM5HACKNSLASH_API APlayerCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	APlayerCharacter();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;
    
    virtual void EquipWeapon(TSubclassOf<AWeapon> WeaponClass);
    
	bool IsAttacking(){ return bIsAttacking; }
	bool IsJumping(){ return bIsJumping; }

protected:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    USpringArmComponent* CameraBoom;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    UCameraComponent* Camera;
    
	UPROPERTY(Replicated)
    FVector CurrentRotationInput;
    
    AWeapon* EquippedWeapon;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    TSubclassOf<AWeapon> DefaultWeapon;

	UPROPERTY(Replicated)
	bool bIsAttacking;

	UPROPERTY(Replicated)
	bool bIsJumping;
    
    bool bCanMove;
    float CachedMaximumWalkSpeed;
    
protected:
    virtual void MoveForward(float AxisValue);
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void Server_MoveForward(float AxisValue);
	virtual bool Server_MoveForward_Validate(float AxisValue);
	virtual void Server_MoveForward_Implementation(float AxisValue);

    virtual void MoveRight(float AxisValue);
	UFUNCTION(Server, WithValidation, Reliable)
	virtual void Server_MoveRight(float AxisValue);
	virtual bool Server_MoveRight_Validate(float AxisValue);
	virtual void Server_MoveRight_Implementation(float AisValue);

    virtual FRotator CalculateTargetRotation();
    virtual void JumpPressed();
    virtual void JumpReleased();

	UFUNCTION(Server, WithValidation, Reliable)
	virtual void Server_Jumping(bool bClientIsJumping);
	virtual bool Server_Jumping_Validate(bool bClientIsJumping);
	virtual void Server_Jumping_Implementation(bool bClientIsJumping);

    virtual void AttackPressed();
    virtual void AttackReleased();

	UFUNCTION(Server, WithValidation, Reliable)
	virtual void Server_Attacking(bool bClientIsAttacking);
	virtual bool Server_Attacking_Validate(bool bClientIsAttacking);
	virtual void Server_Attacking_Implementation(bool bClientIsAttacking);
};
