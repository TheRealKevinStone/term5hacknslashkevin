// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "PlayerCharacter.h"
#include "UnrealNetwork.h"
#include "PlayerAnimInstance.h"


// Sets default values
APlayerCharacter::APlayerCharacter()
    :Super()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->AttachTo(RootComponent);
    
    Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    Camera->AttachTo(CameraBoom);

}

// Called when the game starts or when spawned
void APlayerCharacter::BeginPlay()
{
	Super::BeginPlay();
    //bCanMove = true;
    
    UCharacterMovementComponent* MovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent());
    CachedMaximumWalkSpeed = MovementComponent->MaxWalkSpeed;
    
    
    if(DefaultWeapon != NULL)
    {
        EquipWeapon(DefaultWeapon);
    }
	
}

// Called every frame
void APlayerCharacter::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
    
    float Movement = 0.f;
    
    if (CurrentRotationInput.X != 0.f || CurrentRotationInput.Y != 0.f)
    {
        Movement = 1.f;
    }
    
    AddMovementInput(GetActorForwardVector(), Movement);
    SetActorRotation(FMath::Lerp(GetActorRotation(),CalculateTargetRotation(),0.5f));
}

void APlayerCharacter::EquipWeapon(TSubclassOf<AWeapon> WeaponClass)
{
    if (EquippedWeapon != NULL && EquippedWeapon->IsA(WeaponClass))
    {
        return;
    }
    FActorSpawnParameters SpawnParameters;
    SpawnParameters.Owner = this;
	//SpawnParameters.Instigator;
    EquippedWeapon = GetWorld()->SpawnActor<AWeapon>(WeaponClass, FVector::ZeroVector, FRotator::ZeroRotator, SpawnParameters);
    EquippedWeapon->AttachRootComponentTo(GetMesh(), TEXT("RightHand"));
}

// Called to bind functionality to input
void APlayerCharacter::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
    
    InputComponent->BindAction(TEXT("Attack"), IE_Pressed, this, &APlayerCharacter::AttackPressed);
    InputComponent->BindAction(TEXT("Attack"), IE_Released, this, &APlayerCharacter::AttackReleased);
    InputComponent->BindAxis(TEXT("MoveForward"), this, &APlayerCharacter::MoveForward);
    InputComponent->BindAxis(TEXT("MoveRight"), this, &APlayerCharacter::MoveRight);
    InputComponent->BindAction(TEXT("Jump"), IE_Pressed, this, &APlayerCharacter::JumpPressed);
    InputComponent->BindAction(TEXT("Jump"), IE_Released, this, &APlayerCharacter::JumpReleased);

}

void APlayerCharacter::MoveForward(float AxisValue)
{
    CurrentRotationInput.X = AxisValue;
    CalculateTargetRotation();

	Server_MoveForward(AxisValue);
}

bool APlayerCharacter::Server_MoveForward_Validate(float AxisValue)
{
	return FMath::Abs(AxisValue) <= 1 ? true : false;
}

void APlayerCharacter::Server_MoveForward_Implementation(float AxisValue)
{
	CurrentRotationInput.X = AxisValue;
}

void APlayerCharacter::MoveRight(float AxisValue)
{
    CurrentRotationInput.Y = AxisValue;
    CalculateTargetRotation();

	Server_MoveRight(AxisValue);
}

bool APlayerCharacter::Server_MoveRight_Validate(float AxisValue)
{
	return FMath::Abs(AxisValue) <= 1 ? true : false;
}

void APlayerCharacter::Server_MoveRight_Implementation(float AxisValue)
{
	CurrentRotationInput.Y = AxisValue;
}

FRotator APlayerCharacter::CalculateTargetRotation()
{
    float Yaw = FMath::RadiansToDegrees(FMath::Atan2(CurrentRotationInput.Y, CurrentRotationInput.X));
    
    if (Yaw != 0.f)
        return FRotator(0.f, Yaw, 0.f);
    else if (CurrentRotationInput.X > 0.f)
        return FRotator (0.f, 0.f, 0.f);
    else
        return GetActorRotation();
}

void APlayerCharacter::JumpPressed()
{
    if (CanJump())
    {
        UCharacterMovementComponent* MovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent());
        MovementComponent->JumpZVelocity = GetVelocity().Size() <= 0.f ? 250.f :  400.f;
        
        //UPlayerAnimInstance* AnimInstance = Cast<UPlayerAnimInstance>(GetMesh()->GetAnimInstance());
        //AnimInstance->bEnablePlayerJump = true;

		bIsJumping = true;
		Server_Jumping(bIsJumping);
    }
}

void APlayerCharacter::JumpReleased()
{
	bIsJumping = false;
	Server_Jumping(bIsJumping);
}

bool APlayerCharacter::Server_Jumping_Validate(bool bClientIsJumping)
{
	return true;
}

void APlayerCharacter::Server_Jumping_Implementation(bool bClientIsJumping)
{
	bIsJumping = bClientIsJumping;
}

void APlayerCharacter::AttackPressed()
{
    //UPlayerAnimInstance* AnimInstance = Cast<UPlayerAnimInstance>(GetMesh()->GetAnimInstance());
    //AnimInstance->bEnablePlayerAttack = true;
    //AnimInstance->Speed = 0.f;

	bIsAttacking = true;

	Server_Attacking(bIsAttacking);

	UCharacterMovementComponent* MovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent());
    MovementComponent->MaxWalkSpeed = 0.f;
}

void APlayerCharacter::AttackReleased()
{
	//UPlayerAnimInstance* AnimInstance = Cast<UPlayerAnimInstance>(GetMesh()->GetAnimInstance());
    //AnimInstance->bEnablePlayerAttack = false;

	bIsAttacking = false;

	Server_Attacking(bIsAttacking);


    UCharacterMovementComponent* MovementComponent = Cast<UCharacterMovementComponent>(GetMovementComponent());
    MovementComponent->MaxWalkSpeed = CachedMaximumWalkSpeed;
}

bool APlayerCharacter::Server_Attacking_Validate(bool bClientIsAttacking)
{
	return true;
}

void APlayerCharacter::Server_Attacking_Implementation(bool bClientIsAttacking)
{
	bIsAttacking = bClientIsAttacking;
}

void APlayerCharacter::GetLifetimeReplicatedProps(TArray <FLifetimeProperty> & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerCharacter, CurrentRotationInput);
	DOREPLIFETIME(APlayerCharacter, bIsAttacking);

}