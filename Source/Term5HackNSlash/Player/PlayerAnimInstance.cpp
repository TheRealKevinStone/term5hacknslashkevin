// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "PlayerCharacter.h"
#include "PlayerAnimInstance.h"

void UPlayerAnimInstance::NativeUpdateAnimation(float DeltaTime)
{
    //Super::NativeUpdateAnimation(<#DeltaTime#>);
    
	APlayerCharacter* Owner = Cast<APlayerCharacter>(TryGetPawnOwner());
    if (Owner)
    {
        Speed = Owner->GetVelocity().Size();
		if (Owner->IsAttacking())
		{
			bEnablePlayerAttack = true;
		}
		if (Owner->IsJumping())
		{
			bEnablePlayerJump = true;
		}
    }
}


