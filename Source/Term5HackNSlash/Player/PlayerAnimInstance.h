// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Animation/AnimInstance.h"
#include "PlayerAnimInstance.generated.h"

/**
 * 
 */
UCLASS()
class TERM5HACKNSLASH_API UPlayerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool bEnablePlayerJump;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    float Speed;
    
    UPROPERTY(EditAnywhere, BlueprintReadWrite)
    bool bEnablePlayerAttack;
    
public:
    virtual void NativeUpdateAnimation(float DeltaTime) override;
};
