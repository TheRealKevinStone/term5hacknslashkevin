// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DungeonRoom.h"
#include "DungeonSettings.h"
#include "DungeonGenerator.generated.h"

UCLASS()
class TERM5HACKNSLASH_API ADungeonGenerator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADungeonGenerator();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<UDungeonSettings> DungeonSettingsClass;
	
private:
	UDungeonSettings* DungeonSettings;
	FRandomStream GenerationStream;
	TArray<TSubclassOf<ADungeonRoom>> GridArea;

	int32 CreatedRooms;

private:
	FVector GetLocationFromIndex(int32 Index);
	
	void CreateSpawnRoom();
	void CreateRoom(TSubclassOf<ADungeonRoom> RoomClass, FVector Location, bool bIsNorthRoom);
	void ConnectExit(USceneComponent* Exit);
	void ConnectNorthRoom(FVector CurrentRoomLocation, float Offset);
	void ConnectEastRoom(FVector CurrentRoomLocation, float Offset);

	bool bCanRoomFit(TSubclassOf<ADungeonRoom> RoomClass, FVector Location);
};
