// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "DungeonRoom.generated.h"

UCLASS()
class TERM5HACKNSLASH_API ADungeonRoom : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector2D Dimensions;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FColor DebugColor;

public:	
	// Sets default values for this actor's properties
	ADungeonRoom();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	
	
};
