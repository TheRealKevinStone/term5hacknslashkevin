// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "DungeonRoom.h"
#include "DungeonSettings.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class TERM5HACKNSLASH_API UDungeonSettings : public UObject
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<TSubclassOf<ADungeonRoom>> AvailableRooms;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TSubclassOf<ADungeonRoom> StartRoomClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int32 MaxNumberOfRooms;

public:
	UDungeonSettings();
	
};
