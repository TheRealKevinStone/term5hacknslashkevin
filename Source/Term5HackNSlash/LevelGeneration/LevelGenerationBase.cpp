// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "LevelGenerationBase.h"


// Sets default values
ALevelGenerationBase::ALevelGenerationBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Collider = CreateDefaultSubobject<UBoxComponent>(TEXT("Collider"));
	RootComponent = Collider;

	FloorMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//FloorMesh->AttachTo(RootComponent);

	//WallMesh1 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh1->AttachTo(RootComponent);

	//WallMesh2 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh2->AttachTo(RootComponent);

	//WallMesh3 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh3->AttachTo(RootComponent);

	//WallMesh4 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh4->AttachTo(RootComponent);

	//WallMesh5 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh5->AttachTo(RootComponent);

	//WallMesh6 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh6->AttachTo(RootComponent);

	//WallMesh7 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh7->AttachTo(RootComponent);

	//WallMesh8 = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	//WallMesh8->AttachTo(RootComponent);

}

// Called when the game starts or when spawned
void ALevelGenerationBase::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelGenerationBase::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

