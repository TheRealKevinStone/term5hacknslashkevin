// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "DungeonGenerator.h"


// Sets default values
ADungeonGenerator::ADungeonGenerator()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//GenerationStream = FRandomStream(1);
	GenerationStream = FRandomStream(FDateTime::Now().ToUnixTimestamp());
}

// Called when the game starts or when spawned
void ADungeonGenerator::BeginPlay()
{
	Super::BeginPlay();
	
	check(DungeonSettingsClass);
	DungeonSettings = NewObject<UDungeonSettings>(this,
		DungeonSettingsClass, TEXT("DungeonSettings"));

	CreatedRooms = 0;
	CreateSpawnRoom();
}

// Called every frame
void ADungeonGenerator::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

FVector ADungeonGenerator::GetLocationFromIndex(int32 Index)
{
	return FVector::ZeroVector;
}

void ADungeonGenerator::CreateSpawnRoom()
{
	int32 RoomIndex = FMath::FloorToInt(GenerationStream.FRandRange(0, DungeonSettings->AvailableRooms.Num()));
	TSubclassOf<ADungeonRoom> RoomClass = DungeonSettings->AvailableRooms[RoomIndex];

	ADungeonRoom* Room = GetWorld()->SpawnActor<ADungeonRoom>(RoomClass, FVector::ZeroVector, FRotator::ZeroRotator);
	CreatedRooms++;

	if (CreatedRooms < DungeonSettings->MaxNumberOfRooms)
	{
		float NorthOffset = Room->Dimensions.Y * 0.5f;
		float EastOffset = Room->Dimensions.X * 0.5f;

		ConnectNorthRoom(FVector::ZeroVector, NorthOffset);
		ConnectEastRoom(FVector::ZeroVector, EastOffset);
	}
}

void ADungeonGenerator::CreateRoom(TSubclassOf<ADungeonRoom> RoomClass, FVector Location, bool bIsNorthRoom)
{
	ADungeonRoom* Room = GetWorld()->SpawnActor<ADungeonRoom>(RoomClass, Location, FRotator::ZeroRotator);
	CreatedRooms++;

	if (bIsNorthRoom)
	{
		float Offset = Room->Dimensions.Y * 0.5;
		Location.Y += Offset;

		Room->SetActorLocation(Location);
	}
	else
	{
		float Offset = Room->Dimensions.X * 0.5;

		Location.X += Offset;

		Room->SetActorLocation(Location);
	}


	if (CreatedRooms < DungeonSettings->MaxNumberOfRooms)
	{
		/*TArray<UActorComponent*> SceneComponents = Room->GetComponents();

		for (UActorComponent* Component : SceneComponents)
		{
			if (Component->ComponentHasTag(TEXT("DOOR")))
			{
				ConnectExit(Cast<USceneComponent>(Component));
			}
		}*/
		float NorthOffset = Room->Dimensions.Y * 0.5f;
		float EastOffset = Room->Dimensions.X * 0.5f;

		ConnectNorthRoom(Location, NorthOffset);
		ConnectEastRoom(Location, EastOffset);
	}
}

void ADungeonGenerator::ConnectNorthRoom(FVector CurrentRoomLocation, float Offset)
{
	int32 RoomIndex = FMath::FloorToInt(GenerationStream.FRandRange(0, DungeonSettings->AvailableRooms.Num()));

	TSubclassOf<ADungeonRoom> RoomClass = DungeonSettings->AvailableRooms[RoomIndex];
	FVector Origin;
	FVector BoundsExtent;
	FVector Location = CurrentRoomLocation;
	Location.Z = 0.f;
	Location.Y += Offset;

	//TSubclassOf<ADungeonRoom*> Room = DungeonSettings->AvailableRooms[RoomIndex];
	//float CurrentRoomOffset = Room->Dimensions.Y * 0.5;

	CreateRoom(RoomClass, Location, true);
}

void ADungeonGenerator::ConnectEastRoom(FVector CurrentRoomLocation, float Offset)
{
	int32 RoomIndex = FMath::FloorToInt(GenerationStream.FRandRange(0, DungeonSettings->AvailableRooms.Num()));

	TSubclassOf<ADungeonRoom> RoomClass = DungeonSettings->AvailableRooms[RoomIndex];
	FVector Origin;
	FVector BoundsExtent;
	FVector Location = CurrentRoomLocation;
	Location.Z = 0.f;
	Location.X += Offset;

	CreateRoom(RoomClass, Location, false);
}

void ADungeonGenerator::ConnectExit(USceneComponent* Exit)
{
	////////////////////
	// NOT BEING USED //
	////////////////////
	int32 RoomIndex = FMath::FloorToInt(GenerationStream.FRandRange(0, DungeonSettings->AvailableRooms.Num()));

	TSubclassOf<ADungeonRoom> RoomClass = DungeonSettings->AvailableRooms[RoomIndex];
	FVector Origin;
	FVector BoundsExtent;
	FVector Location = Exit->GetComponentLocation();
	//Location.X += RoomClass->Dimensions.X;
	Location.Z = 0.f;

	CreateRoom(RoomClass, Location, false);
}

bool ADungeonGenerator::bCanRoomFit(TSubclassOf<ADungeonRoom> RoomClass, FVector Location)
{
	return true;
}
