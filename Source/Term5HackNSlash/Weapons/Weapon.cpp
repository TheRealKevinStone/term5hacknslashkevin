// Fill out your copyright notice in the Description page of Project Settings.

#include "Term5HackNSlash.h"
#include "Player/PlayerCharacter.h"
#include "Weapon.h"


// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
    
    Collider = CreateDefaultSubobject<UCapsuleComponent> (TEXT("Collider"));
    Collider->AttachTo(RootComponent);
    Collider->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnWeaponHit);
    
    Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    Mesh->AttachTo(RootComponent);
	//Mesh->OnComponentBeginOverlap.AddDynamic(this, &AWeapon::OnWeaponHit);

	ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	ParticleSystem->AttachTo(RootComponent);
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AWeapon::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void AWeapon::OnWeaponHit(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != GetOwner())
	{
		GEngine->AddOnScreenDebugMessage(1, 1.5f, FLinearColor::Yellow, FString::Printf(TEXT("Weapon Hit %s"), *OtherActor->GetName()));
			//AddOnScreenDebugMessage(1, 5.f, FLinearColor::Yellow, TEXT("Weapon Hit!"));
		APlayerCharacter* Player = Cast<APlayerCharacter>(GetOwner());
		if (Player != NULL)
		{
			UGameplayStatics::ApplyDamage(OtherActor, 10.f, Player->GetController(), Player, UDamageType::StaticClass());
		}
	}
}
